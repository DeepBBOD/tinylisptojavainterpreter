public class WhileStmnt extends Node{
    public Node Left;
    public Node Right;

    public WhileStmnt(Node Left, Node Right) {
        this.Left = Left;
        this.Right = Right;
    }
}