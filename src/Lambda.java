public class Lambda extends Node {
    public LispList Params;
    public Node Body;

    public Lambda(LispList params, Node body) {
        this.Params = params;
        this.Body = body;
    }
}
