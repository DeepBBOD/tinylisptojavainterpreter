public class CondStmnt extends Node{
    public Node Left;
    public Node Middle;
    public Node Right;

    public CondStmnt(Node Left, Node Middle, Node Right) {
        this.Left = Left;
        this.Middle = Middle;
        this.Right = Right;
    }

    public CondStmnt(Node Left, Node Middle){
        this.Left = Left;
        this.Middle = Middle;
        this.Right = null;
    }
}