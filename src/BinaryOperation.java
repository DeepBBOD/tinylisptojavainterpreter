public class BinaryOperation extends Expression {
    public String OpType;
    public Node Left;
    public Node Right;

    public BinaryOperation(String opType, Node left, Node right) {
        this.OpType = opType;
        this.Left = left;
        this.Right = right;
    }

    public Node evaluate() {

        OpType = OpType.toLowerCase();

        Expression left = (Expression) Left.evaluate();
        Expression right = (Expression) Right.evaluate();

        Object l_value = -1.337d;
        Object r_value = .0f;
        String op_class_type = "float";

        // If there is real type in the expression
        if (left.getClass() == RealExpression.class || right.getClass() == RealExpression.class) {
            if (left.getClass() == BinaryExpression.class || right.getClass() == BinaryExpression.class) {
                System.err.println("Boolean and real types cannot be in one expression!");
                return null;
            }
            l_value = left.Value.getClass() == Integer.class ? ((Integer) left.Value) + 0.0f : left.Value;
            r_value = right.Value.getClass() == Integer.class ? ((Integer) right.Value) + 0.0f : right.Value;
            op_class_type = "float";
        }
        // If there is real type in the expression
        else if (left.getClass() == IntExpression.class || right.getClass() == IntExpression.class) {
            if (left.getClass() == BinaryExpression.class || right.getClass() == BinaryExpression.class) {
                System.err.println("Boolean and integer types cannot be in one expression!");
                return null;
            }
            l_value = left.Value;
            r_value = right.Value;
            op_class_type = "int";
        }

        switch (OpType) {
            case "plus": {
                switch (op_class_type) {
                    case "float": {
                        return new RealExpression((float) l_value + (float) r_value);
                    }
                    case "int": {
                        return new IntExpression((int) l_value + (int) r_value);
                    }
                    default: {
                        return null;
                    }
                }
            }
            case "minus": {
                switch (op_class_type) {
                    case "float": {
                        return new RealExpression((float) l_value - (float) r_value);
                    }
                    case "int": {
                        return new IntExpression((int) l_value - (int) r_value);
                    }
                    default: {
                        return null;
                    }
                }
            }
            case "times": {
                switch (op_class_type) {
                    case "float": {
                        return new RealExpression((float) l_value * (float) r_value);
                    }
                    case "int": {
                        return new IntExpression((int) l_value * (int) r_value);
                    }
                    default: {
                        return null;
                    }
                }
            }
            case "divide": {
                switch (op_class_type) {
                    case "float": {
                        if ((float) r_value == 0.0f) {
                            System.err.println("Zero division!!!");
                            return null;
                        }
                        return new RealExpression((float) l_value / (float) r_value);
                    }
                    case "int": {
                        if ((int) r_value == 0) {
                            System.err.println("Zero division!!!");
                            return null;
                        }
                        return new IntExpression((int) l_value / (int) r_value);
                    }
                    default: {
                        return null;
                    }
                }
            }
            default: {
                return null;
            }
        }
    }
}
