public class ProgStmnt extends Node {
    public LispList AtomsToEval;
    public Node Body;

    public ProgStmnt(LispList atomsToEval, Node body) {
        this.AtomsToEval = atomsToEval;
        this.Body = body;
    }
}
