public class ListOp extends Node {
    public String ListOp;
    public LispList List;
    public LispList SecondList;

    public ListOp(String listOp, LispList list) {
        this.ListOp = listOp;
        this.List = list;
    }

    public ListOp(String listOp, LispList list, LispList secondList) {
        this.ListOp = listOp;
        this.List = list;
        this.SecondList = secondList;
    }
}
