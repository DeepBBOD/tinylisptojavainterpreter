public class FuncStmnt extends Node {
    public Atom FuncName;
    public LispList Param;
    public Node Element;

    public FuncStmnt(Atom funcName, LispList param, Node element) {
        this.FuncName = funcName;
        this.Param = param;
        this.Element = element;
    }
}