public class RealExpression extends Expression<Float> {

    public RealExpression(float value) {
        this.Value = value;
    }

    public RealExpression evaluate() { return this; }

    public String toString() { return String.valueOf(Value); }

    public RealExpression plus(RealExpression expr) {
        return new RealExpression(Value + expr.Value);
    }
}
