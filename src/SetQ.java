public class SetQ extends Node {
    public Atom LeftPart;
    public Node RightPart;

    public SetQ(Atom leftPart, Node rightPart){
        this.LeftPart = leftPart;
        this.RightPart = rightPart;
    }
}
