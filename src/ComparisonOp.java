public class ComparisonOp extends Node {
    public String CompOp;
    public Node Left;
    public Node Right;

    public ComparisonOp(String compOp, Node left, Node right) {
        this.CompOp = compOp;
        this.Left = left;
        this.Right = right;
    }
}
