public class PredicateStmnt extends Node {
    public String OpType;
    public Node Left;

    public PredicateStmnt(String opType, Node Left) {
        this.OpType = opType;
        this.Left = Left;
    }
}