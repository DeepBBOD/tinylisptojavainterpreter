import java_cup.runtime.DefaultSymbolFactory;
import java_cup.runtime.Symbol;
import java_cup.runtime.SymbolFactory;


class Main {

    public static void main(String[] argv) throws Exception {
        System.out.println("Waiting for a command...");
        Parser p = new Parser(new scanner());
        //p.parse();
        p.parse();
    }

}

public class scanner {

    public Node RootNode;

    /* single lookahead character */
    private static int next_char;
    private int i_val = 0;

    private SymbolFactory sf = new DefaultSymbolFactory();

    private void advance() throws java.io.IOException {
        next_char = System.in.read();
    }

    void init() throws java.io.IOException {
        advance();
    }

    Symbol next_token() throws java.io.IOException {
        for (; ; )

            if (next_char >= '0' && next_char <= '9') {
                i_val = 0;

                do {
                    i_val = i_val * 10 + (next_char - '0');
                    advance();
                } while (next_char >= '0' && next_char <= '9');

                return sf.newSymbol("NUMBER", sym.NUMBER, i_val);

            } else if (next_char == '.') {

                advance();
                return sf.newSymbol("DOT", sym.DOT);

            } else if (next_char == '+') {

                advance();
                return sf.newSymbol("PLUS_SIGN", sym.PLUS_SIGN);

            } else if (next_char == '-') {

                advance();
                return sf.newSymbol("MINUS", sym.MINUS_SIGN);

            } else if (next_char == '(') {

                advance();
                return sf.newSymbol("LPAREN", sym.LPAREN);

            } else if (next_char == ')') {

                advance();
                return sf.newSymbol("RPAREN", sym.RPAREN);

            } else if ((next_char >= 'a' && next_char <= 'z') || (next_char >= 'A' && next_char <= 'Z')) {

                StringBuilder identifier = new StringBuilder();

                do {

                    identifier.append(Character.toString(next_char));
                    advance();

                } while ((next_char >= 'a' && next_char <= 'z') || (next_char >= '0' && next_char <= '9') || (next_char >= 'A' && next_char <= 'Z'));

                String parsed = identifier.toString();

                switch (parsed) {

                    case "plus":
                        return sf.newSymbol("PLUS", sym.PLUS);

                    case "minus":
                        return sf.newSymbol("MINUS", sym.MINUS);

                    case "times":
                        return sf.newSymbol("TIMES", sym.TIMES);

                    case "divide":
                        return sf.newSymbol("DIVIDE", sym.DIVIDE);

                    case "head":
                        return sf.newSymbol("HEAD", sym.HEAD);

                    case "tail":
                        return sf.newSymbol("TAIL", sym.TAIL);

                    case "cons":
                        return sf.newSymbol("CONS", sym.CONS);

                    case "equal":
                        return sf.newSymbol("EQUAL", sym.EQUAL);

                    case "nonequal":
                        return sf.newSymbol("NONEQUAL", sym.NONEQUAL);

                    case "less":
                        return sf.newSymbol("LESS", sym.LESS);

                    case "lesseq":
                        return sf.newSymbol("LESSEQ", sym.LESSEQ);

                    case "greater":
                        return sf.newSymbol("GREATER", sym.GREATER);

                    case "greatereq":
                        return sf.newSymbol("GREATEREQ", sym.GREATEREQ);

                    case "isint":
                        return sf.newSymbol("ISINT", sym.ISINT);

                    case "isreal":
                        return sf.newSymbol("ISREAL", sym.ISREAL);

                    case "isbool":
                        return sf.newSymbol("ISBOOL", sym.ISBOOL);

                    case "isnull":
                        return sf.newSymbol("ISNULL", sym.ISNULL);

                    case "isatom":
                        return sf.newSymbol("ISATOM", sym.ISATOM);

                    case "islist":
                        return sf.newSymbol("ISLIST", sym.ISLIST);

                    case "and":
                        return sf.newSymbol("AND", sym.AND);

                    case "or":
                        return sf.newSymbol("OR", sym.OR);

                    case "xor":
                        return sf.newSymbol("XOR", sym.XOR);

                    case "not":
                        return sf.newSymbol("NOT", sym.NOT);

                    case "eval":
                        return sf.newSymbol("EVAL", sym.EVAL);

                    case "true":
                        return sf.newSymbol("TRUE", sym.TRUE);

                    case "false":
                        return sf.newSymbol("FALSE", sym.FALSE);

                    case "null":
                        return sf.newSymbol("NULL", sym.NULL);

                    case "quote":
                        return sf.newSymbol("QUOTE", sym.QUOTE);

                    case "\'":
                        return sf.newSymbol("QUOTE_SIGN", sym.QUOTE_SIGN);

                    case "setq":
                        return sf.newSymbol("SETQ", sym.SETQ);

                    case "func":
                        return sf.newSymbol("FUNC", sym.FUNC);

                    case "lambda":
                        return sf.newSymbol("LAMBDA", sym.LAMBDA);

                    case "prog":
                        return sf.newSymbol("PROG", sym.PROG);

                    case "cond":
                        return sf.newSymbol("COND", sym.COND);

                    case "while":
                        return sf.newSymbol("WHILE", sym.WHILE);

                    case "return":
                        return sf.newSymbol("RETURN", sym.RETURN);

                    case "break":
                        return sf.newSymbol("BREAK", sym.BREAK);

                    default:
                        return sf.newSymbol("ID", sym.ID, parsed);

                }

            } else if (next_char == ' ') {

                advance();

            } else {

                advance();
            }

    }
}