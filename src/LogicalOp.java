public class LogicalOp extends Node {
    public String LogicalOp;
    public Node Left;
    public Node Right;

    public LogicalOp(String LOp, Node left, Node right) {
        this.LogicalOp = LOp;
        this.Left = left;
        this.Right = right;
    }
}
