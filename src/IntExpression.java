public class IntExpression extends Expression<Integer> {

    public IntExpression(int value) {
        this.Value = value;
    }

    public IntExpression evaluate() {
        return this;
    }

    public String toString() {
        return Value + "";
    }

    public IntExpression plus(IntExpression expr) {
        return new IntExpression(Value + expr.Value);
    }
}
